CXX = g++
CXXFLAGS= -O3 -ffast-math -Wno-deprecated 
LDFLAGS=-lm -llapack -lblas

prefix = .
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin

srcdir= ./src


svm: mkdirBin $(srcdir)/svm.o $(srcdir)/trainsvm.o $(srcdir)/multiclass.o $(srcdir)/rgenerator.o $(srcdir)/loadup_parameters.o
	$(CXX) $(CXXFLAGS) -o $(bindir)/svm  $(srcdir)/trainsvm.o $(srcdir)/svm.o $(srcdir)/multiclass.o $(srcdir)/loadup_parameters.o  $(srcdir)/rgenerator.o $(LDFLAGS)  -lgfortran

cross: mkdirBin $(srcdir)/svm.o $(srcdir)/cross.o $(srcdir)/multiclass.o $(srcdir)/rgenerator.o $(srcdir)/loadup_parameters.o
	$(CXX) $(CXXFLAGS) -o $(bindir)/cross  $(srcdir)/cross.o $(srcdir)/svm.o $(srcdir)/multiclass.o $(srcdir)/loadup_parameters.o  $(srcdir)/rgenerator.o $(LDFLAGS)

LOO: $(srcdir)/svm.o $(srcdir)/trainLOO.o $(srcdir)/multiclass.o $(srcdir)/rgenerator.o $(srcdir)/loadup_parameters.o 
	$(CXX) $(CXXFLAGS) -o $(bindir)/LOO  $(srcdir)/trainLOO.o $(srcdir)/svm.o $(srcdir)/multiclass.o $(srcdir)/loadup_parameters.o  $(srcdir)/rgenerator.o $(LDFLAGS)  -lgfortran

CLOO: $(srcdir)/svm.o $(srcdir)/crossLOO.o $(srcdir)/multiclass.o $(srcdir)/rgenerator.o $(srcdir)/loadup_parameters.o 
	$(CXX) $(CXXFLAGS) -o $(bindir)/CLOO  $(srcdir)/crossLOO.o $(srcdir)/svm.o $(srcdir)/multiclass.o $(srcdir)/loadup_parameters.o  $(srcdir)/rgenerator.o $(LDFLAGS)  -lgfortran

ALOO: $(srcdir)/svm.o $(srcdir)/averageLOO.o $(srcdir)/multiclass.o $(srcdir)/rgenerator.o $(srcdir)/loadup_parameters.o 
	$(CXX) $(CXXFLAGS) -o $(bindir)/ALOO  $(srcdir)/averageLOO.o $(srcdir)/svm.o $(srcdir)/multiclass.o $(srcdir)/loadup_parameters.o  $(srcdir)/rgenerator.o $(LDFLAGS)  -lgfortran

explore: $(srcdir)/svm.o $(srcdir)/exploresvm.o $(srcdir)/multiclass.o $(srcdir)/rgenerator.o $(srcdir)/loadup_parameters.o 
	$(CXX) $(CXXFLAGS) -o $(bindir)/explore  $(srcdir)/exploresvm.o $(srcdir)/svm.o $(srcdir)/multiclass.o $(srcdir)/loadup_parameters.o  $(srcdir)/rgenerator.o $(LDFLAGS)

sgd: $(srcdir)/svm.o $(srcdir)/trainsgd.o $(srcdir)/multiclass.o $(srcdir)/rgenerator.o $(srcdir)/loadup_parameters.o 
	$(CXX) $(CXXFLAGS) -o $(bindir)/sgd  $(srcdir)/trainsgd.o $(srcdir)/svm.o $(srcdir)/multiclass.o $(srcdir)/loadup_parameters.o  $(srcdir)/rgenerator.o $(LDFLAGS)

srsvm: $(srcdir)/svm.o $(srcdir)/trainSRsvm.o $(srcdir)/multiclass.o $(srcdir)/rgenerator.o $(srcdir)/loadup_parameters.o 
	$(CXX) $(CXXFLAGS) -o $(bindir)/srsvm  $(srcdir)/trainSRsvm.o $(srcdir)/svm.o $(srcdir)/multiclass.o $(srcdir)/loadup_parameters.o  $(srcdir)/rgenerator.o $(LDFLAGS)

randomize: $(srcdir)/randomize.o $(srcdir)/rgenerator.o
	$(CXX) $(CXXFLAGS) -o $(bindir)/randomize $(srcdir)/randomize.o $(srcdir)/rgenerator.o $(LDFLAGS)


mkdirBin:
	mkdir -p bin

all : mkdirBin svm sgd srsvm

clean:
	rm -fr src/*.o

fromScratch:
	rm -fr *.o $(srcdir)/*.o bin $(srcdir)/*~

$(bindir)/multiclass.o: $(srcdir)/svm.h $(srcdir)/loadup_parameters.h
$(bindir)/svm.o: $(srcdir)/svm.h $(srcdir)/loadup_parameters.h
$(bindir)/trainsvm.o: $(srcdir)/svm.h $(srcdir)/loadup_parameters.h
