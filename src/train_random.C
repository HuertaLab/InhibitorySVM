/**************************************************************/
/************* SVM-Stock ****************************************/
/**************************************************************/
/*    Ramon Huerta            *********************************/
/* ***********************************************************/
/* This is in development****No warranty whatsoever***********/
/* Use at your own risk  *************************************/
/*************************************************************/


#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <iterator>
#include <algorithm>
#include <functional>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cstdio>
#include <cmath> 

using namespace std;

#include "svm-svr.h"
#include "loadup_parameters.h"

// Number of cycles that we skipe without training
// This is done  to save some time
#define NCYCLES 4

int get_month(int);
void parse_command_line(int, char **,svmparam *);
REAL draw_stats(vector<REAL>&,vector<REAL>&,svmparam *);
int check_format(string );
REAL get_std(map<int,REAL> &,REAL &) ;
REAL draw_stats(vector<REAL> & /*potfolio*/,vector<REAL> & /*sp500*/,REAL &);
REAL draw_stats2(vector<REAL> &,svmparam *);
void collectall(map<int,map<int,REAL> > &  ,svmparam *  );


int main(int argc,char *argv[]) {

    for(int iter=0;iter<100;++iter) {
  svmparam kr,krc;
  FILE *ar=NULL,*cr=NULL;
  int tau;
  int tmin,tmax;
  int nfeatures;
  bool start=true;
  map<int,map<int,REAL> > allseries;


  if (argc<4)  bad_cmd();

  parse_command_line(argc, argv,&kr);
  nfeatures=kr.nfeatures=check_format(kr.inputfile);
  get_times(kr.inputfile,tmin,tmax,nfeatures);
  printf("File with times [%d,%d]",tmin,tmax);
  krc=kr;

  
  char out_name[200];
  FILE *aout;
  map<int,REAL> res;
  map<int,REAL> alphas;
  sprintf(out_name,"final_C%4.2f_G%4.3f_B%4.2f_N%d_H%d_R%d_Rcut%f_margin%4.2f.dat",kr.C,kr.gamma,kr.bcut,kr.nportfolio,kr.history,kr.r,kr.return_cut,kr.margin_separation);
  aout=fopen(out_name,"w");
  if (aout==NULL) {
      puts("cannot open resultados.dat for writting");
      exit(0);
  }
  REAL Smin=1000;
  vector<REAL> Sstorage;


  for(kr.mshift=0;kr.mshift<3;++kr.mshift) {

      open_output_file(&ar,kr);
      printf("N features %d\n",nfeatures);
      char name[200];
      sprintf(name,"resultados_C%4.2f_G%4.3f_B%4.2f_N%d_H%d_R%d_Cut%4.2lf_mshift%d.dat",kr.C,kr.gamma,kr.bcut,kr.nportfolio,kr.history,kr.r,kr.return_cut,kr.mshift);
      
      cr=fopen(name,"w");
      if (cr==NULL) {
	  puts("cannot open resultados.dat for writting");
	  exit(0);
      }
      
      REAL tret=0.0,tretg=10.0,retsp=0.0,retspg=10.0;
      vector<REAL> pt,sp;  
      int  nt=0;
      REAL factor=252;
      
    tmin=MMAX(1500,tmin);
      
      //tmin=MMAX(11500,tmin);

      printf("*****************\nStart looping time\n***********\nMonth ref %d\n\n",kr.mshift);
      
      tau=tmin;
      int topmax=tmin+365;
      pt.clear();
      sp.clear();


      int mes;

      puts("Looking for the start point");
      do {
	  kr.tmin=tau;
	  kr.tmax=tau+kr.history;
	  kr.tref=kr.tmax+1;
	  mes=get_month(kr.tref);
	  printf("Starting point %d time %d\n",mes,kr.tref);
	  ++tau;
      } while (mes!=kr.mshift);

	  
	  // Set the minimum time to the previous minimum time
	  tmin=kr.tmin;
	  


	  for(;tau<tmax-kr.history-kr.r;/*tau+=kr.r*/) {

	  kr.tmin=tau;
	  kr.tmax=tau+kr.history;
	  kr.tref=kr.tmax+1;

	  time_t current=kr.tref*24*60*60;
	  //kr.tmax-=250; //shifted by 250 days. Don;t use the recent history too many people traing on that
	  printf("Ref time (S=%d) %s\n",kr.tref,ctime(&current));
	  //    kr.tref=tau+kr.history/2;
	  if ((kr.tref+kr.r)>tmax) {
	      puts("Allowable time exceded");
	      break;
	  }
	  
	  svm_svr A(kr); 
	  
	  
	  vector<int> cl=A.get_allclasses();
	  float overall_error=0.0;
	  int i=0;
	  dichotomy D(cl[i],A,kr);
	  
	      
	  printf("\n\nSolving dichotomy for class %d\n",cl[i]);
	  printf("\n\nThe numbers in parenthesis are the upper bound of the KKT conditions and the # of support vectors\nYou can change the tolerance with -T\n");
	  D.method2(); // Solver using Method 2 (Keerthi et al NECO 2001)
	  printf("Training error %f\n",D.error_rate());
	  puts("Exit training");
	  if (ar!=NULL) fprintf(ar,"%d\t%d\t%4.2f%%\n",tau,i,D.error_rate()*100.0);
	      
	  // We do not need to transfer the solution here
//	  D.transfersolution(kr,A);
	      
	  
	  
	  bool ftr=true;
	  /* {
	  // Make sure everything works fine
	  int i=0;
	  puts("Checking working in proper order");
	  A.loadtest_check(kr,ftr);
	  dichotomy D(cl[i],A,kr);
	  puts("trasnfering the solution");
	  D.placesolution(kr,A,cl[i]);
	  puts("solution placed");
	  
	  
	  D.formtestset(cl[i],A,kr);
	  REAL ets=D.errortestset();
	  printf("Check error %lf \n",ets);
	  D.transfer_test_results(A,cl[i]);
	  puts("End check");
	  }*/

	  
	  
	  ftr=false;
	  puts("Loading test set");
	  
	  map<int,REAL> daily,market;
	  REAL unclassified,success;
	  //for(kr.tref=kr.tmin;kr.tref<kr.tmax;++kr.tref)
	  //  A.loadtest(kr,ftr);
	  //kr.tref=6489;
	  int ncycles=0;
	  //  for(int ncycles=0;ncycles<NCYCLES;++ncycles,kr.tref+=kr.r) {

	      for (int ll=0;ll<8;++ll,kr.tref++){
		  A.loadtest(kr,ftr);
		  
		  //  dichotomy D(cl[i],A,kr);
		  //puts("trasnfering the solution");
		  //D.placesolution(kr,A,cl[i]);
		  //puts("solution placed");
		  
		  D.formtestset(cl[0],A,kr);
		  REAL ets=D.errortestset();
		  D.transfer_test_results(A,cl[0]);
		  
		  
		  puts("grading");
		  daily.clear();
		  success=A.evaluate_graded(unclassified,kr,daily,market);//evaluate_test(unclassified,kr.offset);
		  if (success!=0) break;
		  
	      }
	      puts("exiting grading");
	      printf("test return size %d\n",daily.size());
	      //tret+=success;
	      //pt.push_back(success);
	      // tretg*=(1+success/100);
	      //  ++nt;
	      //retsp+=A.get_sp500(kr.tref);
	      //  sp.push_back(A.get_sp500(kr.tref));
	      //retspg*=(1+A.get_sp500(kr.tref)/100);
	      
	      
	      map<int,REAL> ::iterator d;
	      for(d=daily.begin();d!=daily.end();++d) {
		  if ((d->first>kr.tmax)&&(d->first<=(kr.tmax+kr.r*(ncycles+1)))) {
		      REAL alpha=0.0,beta=0.0;
		      if (d->second>=-1.00) {
		      tret+=d->second;
		      retsp+=market[d->first];
		      ++nt;
		      pt.push_back(100.0*d->second);
		      sp.push_back(100.0*market[d->first]);
		      //printf("%lf \n",d->second);
		      tretg*=(1+d->second);
		      retspg*=(1+market[d->first]);
		      
		      alpha=draw_stats(pt /*potfolio*/,sp /*sp500*/,beta); 
		      
		      alphas[kr.mshift]=alpha;
		      allseries[d->first][kr.mshift]=d->second;
		      } 
		      fprintf(cr,"%d\t\%f\t%f\t%f\t%f\t%f\t%f\n",d->first,tretg,retspg,alpha*252,beta,factor*tret/(REAL)nt*100.0,factor*retsp/(REAL)nt*100.0);fflush(cr);
		      
		  }
	      }
	      //} //End of Loop ncycles
	  
	     
	      
	      printf("\n\nRetorno acumulado %f\n\n",tret/(REAL)nt*100);
	      {
		  int ncycles=4;
		  svmparam krp=kr;
		  krp.tmax-=krp.r*ncycles;
		  krp.tref-=krp.r*ncycles;
		  svm_svr Ap(krp); 
		  Ap.Forward_evaluation(krp,cl,ncycles);
		  kr.C=krp.C;
		  kr.gamma=krp.gamma;
	      }	  
	  
      
	  cl.clear();
	  puts("Exiting SVM-R");
	  // Adjust tau: it might have shifted
	  tau=kr.tref-kr.history-1+kr.r;
      }
      REAL lal;
      res[kr.mshift]=draw_stats(pt,sp,&kr);
      
      
      if (Smin>res[kr.mshift]) {
	  Smin=res[kr.mshift];
      }
      REAL Smean;
      REAL Ssigma=get_std(res,Smean);
      
      REAL Amean;
      REAL Asigma=get_std(alphas,Amean);
      fprintf(aout,"%f %f %f %f %f %f\n",kr.bcut,Smean,Ssigma,Smin,Amean*252,Asigma*252);
      fflush(aout);

      if (ar!=NULL) fclose(ar);

      fclose(cr);
  }

  collectall(allseries,&krc);
    }
  return 1;
}

void collectall(map<int,map<int,REAL> > & a,svmparam *kr) {
    FILE *ar;
    char out_name[100];

    sprintf(out_name,"Acumulated_C%4.2f_G%4.3f_B%4.2f_N%d_H%d_R%d_Rcut%f_margin%4.2f.dat",kr->C,kr->gamma,kr->bcut,kr->nportfolio,kr->history,kr->r,kr->return_cut,kr->margin_separation);
    ar=fopen(out_name,"w");
    if (ar!=NULL) {
	vector<REAL> p;
	int nmax=0;
	map<int,map<int,REAL> > ::iterator i;
	REAL sum=10.0;
	for(i=a.begin();i!=a.end();++i) {
	    map<int,REAL>::iterator d;
	    REAL mean=0.0;
	    int nm=0;
	    for(d=i->second.begin();d!=i->second.end();++d) {
		mean+=d->second;
		++nm;
		if (nm>nmax) nmax=nm;
	    }
	    mean/=(REAL)(nmax);
	    sum*=(1+mean);
	    p.push_back(mean);
	    fprintf(ar,"%d %lf\n",i->first,sum);
	}
	fclose(ar);
	draw_stats2(p,kr);
    }
}


REAL get_std(map<int,REAL> &u,REAL &mean) {
    REAL sum=0;
    map<int,REAL>::iterator i;

    if (u.size()==0) return -1;
    
    for(i=u.begin();i!=u.end();++i) 
	sum+=i->second;
    sum/=(REAL)u.size();
    mean=sum;
    REAL std=0.0;
    for(i=u.begin();i!=u.end();++i) 
	std+=(i->second-sum)*(i->second-sum);
	
    std/=(REAL)u.size();

    std=sqrt(std);
    
    return std;

}

int get_month(int fecha) {

    time_t tt=fecha*(24*60*60);
    struct tm * timeinfo;

    timeinfo = localtime ( &tt );

    return timeinfo->tm_mon;

}

REAL draw_stats(vector<REAL> &p,vector<REAL> &sp,svmparam *kr) {


  int i;
  int N=p.size();

  REAL m=0.0,msp=0.0;

  for(i=0;i<N;++i) {
    m+=p[i];
    msp+=sp[i];    
  }
  m/=(REAL) N;
  msp/=(REAL) N;

  REAL s=0.0,ssp=0.0;
  
  for(i=0;i<N;++i) {
    s+=(p[i]-m)*(p[i]-m);
    ssp+=(sp[i]-msp)*(sp[i]-msp);    
  }

  s=sqrt(s/(REAL) N);
  ssp=sqrt(ssp/(REAL) N);


  REAL sharpe=m/s,sharpesp=msp/ssp;

  REAL dr=1.0,drmax=20000;
  REAL drsp=1.0,drspmax=20000;
  REAL ret=1.0,rsp=1.0;
  REAL rmax=0.0,spmax=0.0;
  REAL rmin=1000000.0,spmin=0.0;
  REAL var=0.0;
  for(i=0;i<N;++i) {
      ret*=(1+p[i]/100);
      var+=ABS(p[i]-m);
      if (ret>rmax) {rmax=ret;rmin=ret;}
      if (ret<rmin) {rmin=ret;}
      rsp*=(1+sp[i]/100);
       if (rsp>spmax) {spmax=rsp;spmin=rsp;}
      if (rsp<spmin) {spmin=rsp;}

      dr=(rmin-rmax)/rmax*100.0;
      drsp=(spmin-spmax)/spmax*100.0;

      if (dr<drmax) drmax=dr;
      if (drsp<drspmax) drspmax=drsp;

  }
  var/=(REAL)N;
  

//  REAL f=365.0/(REAL)kr->r;
  REAL f=252.0;

  FILE *ar;
  char name[200];
  sprintf(name,"stats_C%4.2f_G%4.3f_B%4.2f_N%d_H%d_shitf%d_Rcut%f_margin%4.3f.dat",kr->C,kr->gamma,kr->bcut,kr->nportfolio,kr->history,kr->mshift,kr->return_cut,kr->margin_separation);
  
  ar=fopen(name,"w");

  if (ar!=NULL) {
    
      fprintf(ar,"Training [%d]\n",kr->tmin);
      fprintf(ar,"Data points= %d\n\n",sp.size());
    fprintf(ar,"Mean    : %f, Sp %f\n",m,msp);
    fprintf(ar,"Std     : %f, Sp %f\n",s,ssp);
    fprintf(ar,"Sharpe  : %f, Sp %f\n",sharpe,sharpesp);
    fprintf(ar,"Drawdown: %f, Sp %f\n",drmax,drspmax);
    fprintf(ar,"\n\nAnnualized\n");
    fprintf(ar,"Mean    : %f%%, Sp %f%%\n",m*f,msp*f);
    fprintf(ar,"Std     : %f%%, Sp %f%%\n",s*sqrt(f),ssp*sqrt(f));
    fprintf(ar,"Sharpe  : %f, Sp %f\n",sharpe*f/sqrt(f),sharpesp*f/sqrt(f));
    fprintf(ar,"Consistency : %f\n",m/var);
    fclose(ar);
  }
sprintf(name,"SHARPE_C%4.2f_G%4.3f_B%4.2f_N%d_H%d_rev%d_margin%4.3f.dat",kr->C,kr->gamma,kr->bcut,kr->nportfolio,kr->history,kr->reversal_time,kr->margin_separation);
  
  ar=fopen(name,"w");

  if (ar!=NULL) {
    fprintf(ar,"%lg\n",sharpe*f/sqrt(f),sharpesp*f/sqrt(f));
    fclose(ar);
  }

  return sharpe*f/sqrt(f);
}


void parse_command_line(int argc, char **argv,svmparam *param)
{
	// default values
	param->problem_type="SVM";
	param->pdegree = 3;
	param->c0 = 0;
	param->C = 1.0;
	param->ratio=1.0;
	param->tol = 1e-3;
	param->debug = false;
	param->ftestset =false; // the test set is not present by default
	param->svr_only =false;
	param->svm_only =true;
	param->outputfile="";
	param->modelfile="";
	param->modelfile_present=false;
	param->normalization=true;
	param->turbo=false;

	parse_cmd(argc,argv,param);

	if ((param->modelfile_present==true)&&(param->modelfile.size()==0)) {
	  puts("Do not forget to indicate the model file with -m\n\n");
	  bad_cmd();
	}
	


}

int check_format(string filename) {
 time_t date;
 char   label[8];
 int    last;
 float  val[1000];
 int kl,klgood;
 bool fl=false;

 for(kl=3;kl<800;++kl) { 
   fl=true;
   ifstream ar(filename.c_str(), ios::in | ios::binary);
   //printf("Kl %d \n",kl);
   if (ar.is_open()) {
     for(;;) {
       if(!(ar.read ((char *)(&date), sizeof (time_t)))) break;
       int t=date/(60*60*24);
       if (!((t>-4000)&&(t<25000))) {
	 printf("Bad Fecha %d\n",t);
	 fl=false;
	 break;
       } else {
	   //  printf("Good Fecha %d\n",t);
       }
       
       if(!(ar.read (label, 8))) break;
       string ticker=label;
       if (ticker.size()==0) {
	 printf("wrong ticker");
	 fl=false;
	 break;
       }	 
       // Read flag to check whether the stock is tradable
       int tradedable;
       if(!(ar.read ((char *)(&tradedable), sizeof (int)))) break;
       if ((tradedable<0)||(tradedable>1)) {
	   puts("Bad flag");
	   fl=false;
	   break;
       }
       
       if(!(ar.read ((char *)(&val), kl*sizeof (float)))) break;
       if ((val[0]<0)||(val[1]<0)) {
	 printf("bas stock value %f\n",val[0]);
	 fl=false;
	 break;
       }
       if(!(ar.read ((char *)(&last), sizeof (int)))) break;
       if ((last>10)||(last<-2)) {
	 fl=false;
	 break;
       }
      
       klgood=kl-2;
     }
   
      ar.close();
 
   }
   if (fl) break;
 }

 printf("There are %d features \n",klgood);

 return klgood;

}

REAL draw_stats(vector<REAL> &y /*potfolio*/,vector<REAL> &x /*sp500*/,REAL &beta) 
{
    REAL B[4];
    B[0]=B[1]=B[3]=0.0;

    for(int i=0;i<x.size();++i) {
	B[0]+=1.0;
	B[1]+=x[i];
	B[3]+=x[i]*x[i];
    }

    printf("%f %f %f\n",B[0],B[1],B[2]);

    float A[2];
    int i;

    A[0]=A[1]=0.0;
  
    for(i=0;i<x.size();++i) {
	A[0]+=y[i];
	A[1]+=x[i]*y[i];
    }


    REAL aa,bb;
    REAL dd=(-B[0]*B[3]+B[1]*B[1]);
    if (dd!=0) {
	bb=(-B[0]*A[1]+B[1]*A[0])/dd;
	aa=(-A[0]*B[3]+B[1]*A[1])/dd;
    } else {
	bb=aa=0.0;
    }
//    printf("A=%f B=%f\n",aa,bb);
    beta=bb;
    return aa;

}


REAL draw_stats2(vector<REAL> &p,svmparam *kr) {


  int i;
  int N=p.size();

  REAL m=0.0;

  for(i=0;i<N;++i) {
    m+=p[i];
  }
  m/=(REAL) N;

  REAL s=0.0;
  
  for(i=0;i<N;++i) {
    s+=(p[i]-m)*(p[i]-m);
  }

  s=sqrt(s/(REAL) N);


  REAL sharpe=m/s;

  REAL dr=1.0,drmax=20000;
  REAL ret=1.0,rsp=1.0;
  REAL rmax=0.0,spmax=0.0;
  REAL rmin=1000000.0,spmin=0.0;
  REAL var=0.0;
  for(i=0;i<N;++i) {
      ret*=(1+p[i]/100);
      var+=ABS(p[i]-m);
      if (ret>rmax) {rmax=ret;rmin=ret;}
      if (ret<rmin) {rmin=ret;}

      dr=(rmin-rmax)/rmax*100.0;

      if (dr<drmax) drmax=dr;

  }

//  REAL f=365.0/(REAL)kr->r;
  REAL f=252.0;

  FILE *ar;
  char name[200];
  sprintf(name,"FINALstats_C%4.2f_G%4.3f_B%4.2f_N%d_H%d_Rcut%f_margin%4.3f.dat",kr->C,kr->gamma,kr->bcut,kr->nportfolio,kr->history,kr->return_cut,kr->margin_separation);
  
  ar=fopen(name,"a+");

  if (ar!=NULL) {
    
      fprintf(ar,"Training [%d]\n",kr->tmin);
      //fprintf(ar,"Data points= %d\n\n",p.size());
      // fprintf(ar,"Mean    : %f\n",m);
      //fprintf(ar,"Std     : %f\n",s);
      //fprintf(ar,"Sharpe  : %f\n",sharpe);
    fprintf(ar,"Drawdown: %f\n",drmax);
    fprintf(ar,"\n\nAnnualized\n");
    fprintf(ar,"Mean    : %f \n",m*f);
    fprintf(ar,"Std     : %f\n",s*sqrt(f));
    fprintf(ar,"Sharpe  : %f\n",sharpe*f/sqrt(f));
    fprintf(ar,"Consistency : %f\n",m/var);
    fclose(ar);
  }


  return sharpe*f/sqrt(f);
}


