/**************************************************************/
/************* SVM-SVR ****************************************/
/**************************************************************/
/*    Ramon Huerta            *********************************/



#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <iterator>
#include <algorithm>
#include <functional>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cstdio>
#include <cmath> 

using namespace std;

#include "svm-svr.h"
#include "loadup_parameters.h"

int get_month(int);
void parse_command_line(int, char **,svmparam *);
void draw_stats(vector<REAL>&,vector<REAL>&,svmparam *);
int check_format(string );

int main(int argc,char *argv[]) {
  svmparam kr;
  FILE *ar=NULL,*cr=NULL;
  int tau;
  int tmin,tmax;
  int nfeatures;
  bool start=true;


  if (argc<4)  bad_cmd();

  parse_command_line(argc, argv,&kr);
  nfeatures=kr.nfeatures=check_format(kr.inputfile);
  get_times(kr.inputfile,tmin,tmax,nfeatures);
  printf("File with times [%d,%d]",tmin,tmax);
  open_output_file(&ar,kr);

  
  printf("N features %d\n",nfeatures);
  char name[200];
  sprintf(name,"resultados_C%4.2f_G%4.3f_B%4.2f_N%d_H%d_R%d_K%d_margin%4.2f.dat",kr.C,kr.gamma,kr.bcut,kr.nportfolio,kr.history,kr.r,kr.reversal_time,kr.margin_separation);
  
  cr=fopen(name,"w");
  if (cr==NULL) {
    puts("cannot open resultados.dat for writting");
    exit(0);
  }

  REAL tret=0.0,tretg=10.0,retsp=0.0,retspg=10.0;
  vector<REAL> pt,sp;  
  int  nt=0;
  REAL factor=365.0/(REAL)kr.r;

  tmin=MMAX(6136,tmin);

  printf("*****************\nStart looping time\n***********\nMonth ref %d\n\n",kr.mshift);

  for(tau=tmin;tau<tmax-kr.history;tau+=kr.r) {
      int mes;
      if (start) {
	  puts("Looking for the start point");
	  do {
	      kr.tmin=tau;
	      kr.tmax=tau+kr.history;
	      kr.tref=kr.tmax+1;
	      mes=get_month(kr.tref);
	      printf("Starting point %d time %d\n",mes,kr.tref);
	      ++tau;
	  } while (mes!=kr.mshift);
	  start=false;

      } else {
	  kr.tmin=tau;
	  kr.tmax=tau+kr.history;
	  kr.tref=kr.tmax+1;
      }
      time_t current=kr.tref*24*60*60;
      //kr.tmax-=kr.add; //shifted by 250 days. Don;t use the recent history too many people traing on that
      printf("Ref time (S=%d)(tmax=%d) %s\n",kr.tref,kr.tmax,time(&current));
    //    kr.tref=tau+kr.history/2;
      if ((kr.tref+kr.r)>tmax) {
	  puts("Allowable time exceded");
	  break;
      }


    svm_svr A(kr); 
 

    vector<int> cl=A.get_allclasses();
    
   

    float overall_error=0.0;
    
    int ncount=0;
   
    {
	int i=0;
      dichotomy D(cl[i],A,kr);
      

      printf("\n\nSolving dichotomy for class %d\n",cl[i]);
      printf("\n\nThe numbers in parenthesis are the upper bound of the KKT conditions and the # of support vectors\nYou can change the tolerance with -T\n");
      D.method2(); // Solver using Method 2 (Keerthi et al NECO 2001)
      printf("Training error %f\n",D.error_rate());
      puts("Exit training");
      if (ar!=NULL) fprintf(ar,"%d\t%d\t%4.2f%%\n",tau,i,D.error_rate()*100.0);

      D.transfersolution(kr,A);
    }

    
    bool ftr=true;


    ftr=false;
    puts("Loading test set");
    A.loadtest(kr,ftr);


    {
	int i=0;
      dichotomy D(cl[i],A,kr);
      puts("trasnfering the solution");
      D.placesolution(kr,A,cl[i]);
      puts("solution placed");
    
 
      D.formtestset(cl[i],A,kr);
      REAL ets=D.errortestset();
      D.transfer_test_results(A,cl[i]);
      if (ar!=NULL) fprintf(ar,"(T)%d\t%4.2f%%\n\n",i,ets*100.0);
      overall_error+=ets;
      ++ncount;
  
    }
    //    if (!kr.modelfile_present) A.savesolution(kr);

      if (ar!=NULL) {
	fprintf(ar,"\nOVERALL TEST SET RESULTS\n");
	
	REAL unclassified,success;
	puts("grading");
	success=A.evaluate_graded(unclassified,kr);//evaluate_test(unclassified,kr.offset);
	fprintf(ar,"b-offset=%4.2f\tAccuracy=%4.2f%%\tUnclassified Patterns %4.2f%%\n\n\n",kr.offset,success,unclassified);
	puts("exiting grading");
	tret+=success;
	pt.push_back(success);
	tretg*=(1+success/100);
	++nt;

	retsp+=A.get_sp500(kr.tref);
	sp.push_back(A.get_sp500(kr.tref));
	retspg*=(1+A.get_sp500(kr.tref)/100);

	printf("\n\nRetorno acumulado %f\n\n",tret/(REAL)nt);

	fprintf(cr,"%d\t\%f\t%f\t%f\t%f\n",kr.tref+kr.r,tretg,retspg,factor*tret/(REAL)nt,factor*retsp/(REAL)nt);fflush(cr);
      }


      
    cl.clear();
    puts("Exiting SVM-R");
		
  }
  draw_stats(pt,sp,&kr);
    if (ar!=NULL) fclose(ar);

  fclose(cr);

  return 1;
}

int get_month(int fecha) {

    time_t tt=fecha*(24*60*60);
    struct tm * timeinfo;

    timeinfo = localtime ( &tt );

    return timeinfo->tm_mon;

}

void draw_stats(vector<REAL> &p,vector<REAL> &sp,svmparam *kr) {


  int i;
  int N=p.size();

  REAL m=0.0,msp=0.0;

  for(i=0;i<N;++i) {
    m+=p[i];
    msp+=sp[i];    
  }
  m/=(REAL) N;
  msp/=(REAL) N;

  REAL s=0.0,ssp=0.0;
  
  for(i=0;i<N;++i) {
    s+=(p[i]-m)*(p[i]-m);
    ssp+=(sp[i]-msp)*(sp[i]-msp);    
  }

  s=sqrt(s/(REAL) N);
  ssp=sqrt(ssp/(REAL) N);


  REAL sharpe=m/s,sharpesp=msp/ssp;

  REAL dr=0.0,drmax=20000;
  REAL drsp=0.0,drspmax=20000;

  for(i=0;i<N;++i) {

    if (p[i]<0) dr+=p[i];
    if (sp[i]<0) drsp+=sp[i];

    if (dr<drmax) drmax=dr;
    if (drsp<drspmax) drspmax=drsp;

    if (p[i]>0) dr=0.0;
    if (sp[i]>0) drsp=0.0;
  }

  REAL f=365.0/(REAL)kr->r;

  FILE *ar;
  char name[200];
  sprintf(name,"stats_C%4.2f_G%4.3f_B%4.2f_N%d_H%d_rev%d_margin%4.3f.dat",kr->C,kr->gamma,kr->bcut,kr->nportfolio,kr->history,kr->reversal_time,kr->margin_separation);
  
  ar=fopen(name,"w");

  if (ar!=NULL) {
    
    fprintf(ar,"Mean    : %f, Sp %f\n",m,msp);
    fprintf(ar,"Std     : %f, Sp %f\n",s,ssp);
    fprintf(ar,"Sharpe  : %f, Sp %f\n",sharpe,sharpesp);
    fprintf(ar,"Drawdown: %f, Sp %f\n",drmax,drspmax);
    fprintf(ar,"\n\nAnnualized\n");
    fprintf(ar,"Mean    : %f%%, Sp %f%%\n",m*f,msp*f);
    fprintf(ar,"Std     : %f%%, Sp %f%%\n",s*sqrt(f),ssp*sqrt(f));
    fprintf(ar,"Sharpe  : %f, Sp %f\n",sharpe*f/sqrt(f),sharpesp*f/sqrt(f));
    fclose(ar);
  }


}


void parse_command_line(int argc, char **argv,svmparam *param)
{
	// default values
	param->problem_type="SVM";
	param->pdegree = 3;
	param->c0 = 0;
	param->C = 1.0;
	param->ratio=1.0;
	param->tol = 1e-3;
	param->debug = false;
	param->ftestset =false; // the test set is not present by default
	param->svr_only =false;
	param->svm_only =true;
	param->outputfile="";
	param->modelfile="";
	param->modelfile_present=false;
	param->normalization=true;
	param->turbo=false;

	parse_cmd(argc,argv,param);

	if ((param->modelfile_present==true)&&(param->modelfile.size()==0)) {
	  puts("Do not forget to indicate the model file with -m\n\n");
	  bad_cmd();
	}
	


}

int check_format(string filename) {
 time_t date;
 char   label[8];
 int    last;
 float  val[1000];
 int kl;
 bool fl=false;

 for(kl=3;kl<800;++kl) { 
   fl=true;
   ifstream ar(filename.c_str(), ios::in | ios::binary);
   if (ar.is_open()) {
     for(;;) {
       if(!(ar.read ((char *)(&date), sizeof (time_t)))) break;
       int t=date/(60*60*24);
       if (!((t>-4000)&&(t<15000))) {
	 printf("Bad Fecha %d\n",t);
	 fl=false;
	 break;
       }
       if(!(ar.read (label, 8))) break;
       string ticker=label;
       if (ticker.size()==0) {
	 printf("wrong ticker");
	 fl=false;
	 break;
       }	 

       if(!(ar.read ((char *)(&val), kl*sizeof (float)))) break;
       if ((val[0]<0)||(val[1]<0)) {
	 printf("bas stock value %f\n",val[0]);
	 fl=false;
	 break;
       }
       if(!(ar.read ((char *)(&last), sizeof (int)))) break;
       if ((last>10)||(last<-2)) {
	 fl=false;
	 break;
       }
      
     }
   
      ar.close();
 
   }
   if (fl) break;
 }

 printf("There are %d points\n",kl-2);

 return kl-2;

}
