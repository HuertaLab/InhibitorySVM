/**************************************************************/
/************* SVM-SVR ****************************************/
/**************************************************************/
/*    Ramon Huerta            *********************************/


#define MMIN(X,Y) (((X)<(Y))?(X):(Y))
#define MMAX(X,Y) (((X)>(Y))?(X):(Y))
#define ABS(X)    (((X)>0)?(X):-(X))
#define SGN(X)    (((X)>=0)?(1.0):(-1.0))
#define EPS       1e-6  // Limit of resolution
#define MAXITER   1000000 // Limit in the # of iterations
#define COUNTER   100   // progress indicator


#define FIELDS2 2 
#define EXTRA  0 //(3+6+6+3+5)
#define FUN    13 
//#define AFIELDS (FIELDS2+EXTRA+FUN) // Number of parameters
#define AFIELDS 1000

typedef struct {
  time_t date;
  char  label[8];
  float value;
  float realvalue;
    int tradedable;
  float pv[AFIELDS]; // As given by yahoo: i price i+1 volume
    float std;  
    float alpha;
    float risk_ajusted;
  int error;
} to_be_stored;

int read_field(ifstream & ,vector<float>  &v,int ,string & ,time_t &, float &,float &,int &,int &);

typedef double REAL; // More precission makes the algorithm converge faster
                     // Nevertheless try it out on your machine
//typedef float REAL;

// Data Storage type
typedef struct {
  int cls; // Class type
  REAL d; // data point value that is used for the regression
  int  timestamp;
  char ticker[10];
    int tradedable;
  vector<REAL> F;// Features;
  REAL r;
      REAL rup;
    REAL rdown;
    REAL price;
    map<int,REAL> daily; // contain daily returns for future comparisons
} field; 

 // This data structures will be only used locally
typedef struct {
  int cls; // Class type
  REAL d; // data point value that is used for the regression
  int  timestamp;
  char ticker[10];  
    int tradedable;
  map<int,REAL> F;// Features;
  int M; // Number of features per data point;
  REAL r;
  REAL rup;
    REAL rdown;
    REAL price;
      map<int,REAL> daily; // contain daily returns for future comparisons
} fieldlocal; 


typedef struct {
  int N;              // Number of points per class
  vector<REAL> mean; // Mean for each feature
  vector<REAL> std;  // Std of each feature
} stats;

// Kernel types
enum {LIN,POLY,RBF,SIGMOID};

typedef struct {
  string problem_type; // SVM, SVR or SVMR
  int    kernel_type; // Select function
  int    pdegree;
  REAL   gamma;
  REAL   c0;
  REAL   C;        // C=CSVM+CSVR
  REAL   ratio;    // ratio = CVM/SVR
  REAL   tol;
  REAL   epsilon;  // epsilon-intensive distance
  bool   debug; 
  bool   ftestset; // true for datatest present;
  bool   svr_only;
  bool   svm_only;
  bool   modelfile_present;
  string inputfile;
  string testfile;
  string outputfile;
  string modelfile;
  bool normalization;
  bool turbo;
  REAL offset;
  REAL bcut;
  int  r;           // rebalance period
  int tmin;
  int tmax;
  int tref;
  int history;
  int nportfolio;
  int nfeatures;
  int nclasses; 
  int Pcut;
  int last;
    int mshift;
    REAL margin_separation;
    int reversal_time; //It appears that learning patterns in very recent times
                       // can be potentially used as arbitrage strategy. We may want to avoid it
    int add; // We shift the learning
    REAL return_cut; // If the returns of individual stocks are larger than 
                     //a "return_cut" threshold close the position.
} svmparam;

class dichotomy;

class svm_svr {
 private:
  string problem_type;          // SVM,SVR or SVMR
  vector<field> data;           // Data storage
  int N;                        // Number of data points  
  int M;                        // Dimension of the feature vector
  int Nc;                       // Number of classes
  map<int, stats> Stats;        // Basic statistics of the data per class (class-> stats)
  stats     ALL_dataset;        // Overall stats of the data set 
  int minfeature,maxfeature;
  int shift;
  int tmin,tmax;
  int ttref;
  REAL margin;                  // Margin of confidence
  REAL igamma;
  vector<field> datatest;       // Data for the test set 
  int Ntest;                    // Number of data points  
  int Mtest;                    // Dimension of the feature vector
  int Ntestc;                   // Number of classes
  bool Ftestset;                // True if test file is present for the test file
  REAL offset;                  // Offset value for the multicalssification problem
  int  reversal;                // Recent times induce spurious results. This correct for it
  REAL bcut;
  REAL Rcut;                    // If the returns of individual stocks are larger than 
                                //a "return_cut" threshold close the position.
  int  r;
  int  Nportfolio;
  int Pcut;                     // 0: for pure returns 1: for relative returns
  map<int,REAL> sp500,Imean;
  map<int,REAL> SP500;
  REAL mean_sp500;
  REAL vol_sp500;

  vector<int> blank_vectors;    // Vectors with blank features
  vector<vector<int> > blanks;  // features with blanks

  // The support vectors
  map<int, vector<REAL> > A; // For each class
  map<int, vector<vector<REAL> > > ASV;
  map<int, REAL> B;

  // Results of classifrying the test set
  map<int, vector<REAL> >  C_test; // for classification
  map<int, vector<REAL> >  R_test; // for regression
  // Output file with the results 
  string output_result;

 public:
  svm_svr(svmparam & ); // Load up the data
  svm_svr(svmparam &,int,int);
  svm_svr(svmparam &,int);
  void loadtest(const svmparam &,bool );
  int loadtest_cont(const svmparam &,bool,int );
  void loadtest_check(const svmparam &,bool );
  vector<int> get_allclasses();
  int load_file(const string & filename, vector<fieldlocal> & ,map<int, vector <fieldlocal> > &);
  ~svm_svr();   
  vector<float> get_means(int);
  vector<float> get_stds(int);
  vector<int>  get_nclasses();
  void push_all(int,REAL,vector<REAL> &, vector<vector<REAL> > &);
  void savesolution(const svmparam & );
  void load_model(svmparam &);
  void modelparse(string &,int &,REAL &,int &, svmparam &); 
  REAL getbeta(int);
  vector<REAL> getA(int);
  vector<vector<REAL> > getSV(int);
  void loadup_testresults(vector<REAL> &,vector<REAL> &,int);
  REAL evaluate_test(REAL &,REAL);
  REAL evaluate_test();
  void complement(map<int, vector <fieldlocal> > &);
  void complement2(vector<int> & );
  int  load_stockmarket(const string &, vector<fieldlocal> &,map<int, vector <fieldlocal> > &,int,int);
  int  load_stockmarket_P(const string &, vector<fieldlocal> &,map<int, vector <fieldlocal> > &,int,int);
    int  load_stockmarket_P2(const string &, vector<fieldlocal> &,map<int, vector <fieldlocal> > &,int,int);
  int  load_stockmarket_P_alpha(const string &, vector<fieldlocal> &,map<int, vector <fieldlocal> > &,int,int);
  int  load_stockmarket(const string &, vector<fieldlocal> &,map<int, vector <fieldlocal> > &,int);
   int  load_stockmarket_last(const string &, vector<fieldlocal> &,map<int, vector <fieldlocal> > &,int);
  REAL evaluate_offset(REAL);
  void loadup_sp500();
  REAL get_sp500(int);
  REAL get_Imean(int);
  REAL evaluate_graded(REAL &,svmparam & );
  REAL evaluate_graded(REAL &,svmparam & ,map<int,REAL> &,map<int,REAL> &);
  REAL evaluate_graded_continous(REAL &,svmparam & ,map<int,REAL> &,map<int,REAL> &,vector<int> cl);
  REAL evaluate_gradedlong(REAL &,svmparam & ,map<int,REAL> &,map<int,REAL> &);
   REAL evaluate_graded_limit(REAL &,FILE *); 
  void load_stockmarket_formclasses(const string & ,int );
   void evaluate_graded(FILE *);
  void evaluate_graded_limit(FILE *);
  void Forward_evaluation(svmparam &,vector<int>,int);
  friend class dichotomy;
  
};

 
// This class solves the problem of one given class versus the rest
class dichotomy {
 private:
  int  kernel_type;      
  REAL CSVM;              // The cost value for SVM 
  REAL CSVR;              // The cost value for SVR
  REAL tol;               // Tolerance level for verification of the KKT conditions
  int selected_class;        
  int N;                   // Number of data points in the training set
  int M;                   // Number of features
  int Npositive;           // Number of positive examples
  REAL blow,bup;
  REAL beta;
  REAL epsilon;            // epslion value of the regression
  int iterations;
  bool track_progress;     // Turn this one to true if you want to track progress
  bool turbo;              // Get faster run times. It's useful to make quick models
                           // We will use this pointer to track progress of the SMO
  REAL bcut;
  FILE *aar;
  REAL max_boundary,min_boundary;

  vector<REAL> ahat,a,ap; // $\hat{\alpha}, \alpha, \alpha'$
  set<int> iahat,ia,iap;  // index of nonzero vectors
  REAL parameter_sum;     // fabs(ahat)+fabs(a)+fabs(ap)
  vector<REAL> y;         // y=-1, y=1;
  REAL *xplus;
  REAL *xminus;
  REAL *xcenter;
  
 
  vector<REAL>            d;         // value of the regression if it applies
  REAL         dmean,dsigma;         // Stats of real values
  vector<vector<REAL> >   x;         // data points;
  vector<vector<int> >  snz;         // Nonzero data points
  vector<int>        i_svmr;         // index of the positive examples

  REAL *K;
  long ksize;

  vector<vector<REAL> > SVALL;
  vector<REAL>  aALL;

  REAL error_trainset;
  REAL correlation;
  REAL error_svm;
  REAL RMS;

  //Test set 
  vector<REAL>           ytest; // y=-1, y=1;
  vector<REAL>           dtest; // value of the regression if it applies
  vector<vector<REAL> >  xtest; // data points;
  vector<vector<int> > stestnz; // non-zero index 
  vector<REAL>          c_test; // results of the classification    
  vector<REAL>          r_test; // results of the regression
  REAL                  offset; // offset of the b parameter for the multicassification problem

  //BETA SETS
  int     *BB; // 0=all 1=up 2=low
  int    *BR1; // 0=all 1=up 2=low
  int    *BR2; // 0=all 1=up 2=low
  int  expand; // resize universe
  int compara; // change rules of comparison

  // BETA SETS
  int F_iup,FR_iup,FR_iup2;
  REAL BSVMup,MSVMup,BSVRup;
  int  sup; //sup=1 for SVM index, 2 for SVR index

  int F_ilow,FR_ilow,FR_ilow2;
  REAL BSVMlow,MSVMlow,BSVRlow;
  int slow; // slow=1 for SVM index, 2 for SVR index

  // indexes in  KKT compliance
  set<int> inKKT;

  // Cached Errors: Ehat for SVM E for SVR
  REAL *E,*Ehat;

  // Kernel parameters
  int  pdegree;
  REAL gamma;
  REAL c0;

  REAL klinear(int, int);
  REAL klineartest(int, int);
  REAL kpoly(int, int);
  REAL krbf(int, int);
  REAL krbftest(int, int);
  REAL ksigmoid(int,int);
  REAL kinverse(int,int);

  typedef REAL (dichotomy::*KERNEL)(int,int); 
  KERNEL kernel; 
  KERNEL GetFunctionPointer( int ); 
  KERNEL IP,IP1,IP2;    // pointer to the inner product

  // Pointer to either to FSVM, FSVR or FSVR
  // Just to improve efficeincy
  typedef REAL (dichotomy::*FUNCTION_CALL)(int); 
  FUNCTION_CALL FC;

  // update functions for Es
  typedef void (dichotomy::*UPDATE_CALL_SVM)(int &,int &,REAL &,REAL &); 
  UPDATE_CALL_SVM UC_SVM;

  typedef void (dichotomy::*UPDATE_CALL_SVR)(int &,int &,REAL &,REAL &,REAL &,REAL &); 
  UPDATE_CALL_SVR UC_SVR;

  // This pointer selects the beta searh function for SVM, SVR and SVMR
  typedef void (dichotomy::*BETA_SEARCH)(); 
  BETA_SEARCH BS;

 public:
  
  dichotomy(const int,svm_svr &,const svmparam & );
  ~dichotomy();
  
  REAL ip(int,int); // Inner product
  REAL iptest_SV_SV(int,int);
  REAL iptest_general(int,int);
  REAL ip2(int,int); // Inner product for sparse data
  REAL F(int);
  REAL FSVM(int);
  REAL FSVR(int);
  REAL Ftest(int);

  void smohat(int,int);
  void smoij(int,int);
  void smoij2(int,int);
  void smoij3(int,int);
  void smomixed(int,int);
  void method2();
  void method2_svr();
  void method2_svmr();
  void inBETA(REAL &,REAL &,int &);
  void inBETAR(REAL &,REAL &,int &);
  int verifyKKT(int);
  int variableKKT(int);
  REAL progress();
  void examinesvm( );
  REAL errorrate();
  REAL errorratesvr();
  REAL errorratesvmr();
  REAL simplerrorrate();
  REAL simplerrorratesvr();
  REAL simplerrorratesvmr();
  void updateallEs();                                            // update Es for SVMs
  void updateallEijs(int &,int &,REAL &,REAL &,REAL &,REAL &);   // update E for SVR
  void updateallEijs_full(int &,int &,REAL &,REAL &,REAL &,REAL &);   // update E for SVMR
  REAL getK(int i,int j);
  void updateallEs(int&,int&,REAL&,REAL&);
  void updateallEs_full(int&,int&,REAL&,REAL&);
  void updateEmixed(int &,int &,REAL &,REAL &);
  void update_sum(REAL,REAL,REAL,REAL);            // To update vector weight
  void examinesvr();
  int  examinesvmr();
  int  examinesvmr2();
  void formtestset(const int,svm_svr &,const svmparam & );
  REAL errortestset();
  REAL errortestsetsvr();
  REAL errortestsetsvmr();
  void savesolution(const svmparam &,int);
  void transfersolution(const svmparam & , svm_svr & );
  void placesolution(const svmparam &, svm_svr &,int);
  REAL updatePhi (int &,int &,REAL &,REAL &,REAL &,REAL &,REAL &,REAL &,REAL &,REAL &);
  void saveregressiondata();
  void search_beta_sets_svm();
  void search_beta_sets_svr();
  void search_beta_sets_svmr();
  REAL evalsvr();
  REAL evalsvmr();
  REAL evalsvm();
  REAL rms();                        // Root mean square error
  REAL pearson_correlation();       
  REAL error_rate();                 // Classification error
  int  verifyKKTR(vector<int> &);
  void get_uplow();
  void get_uplowR();
  void transfer_test_results(svm_svr &,int);
   void lasterrorrate();
};



