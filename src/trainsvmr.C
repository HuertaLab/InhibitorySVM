/**************************************************************/
/************* SVM-SVR ****************************************/
/**************************************************************/
/*    Ramon Huerta            *********************************/
/*    Mehmet K.  Muezzinoglu  *********************************/
/**************************************************************/
/** July 290th, 2009                                       ****/
/** Based on "Support vector classifier and regressor"       **/
/**************************************************************/


#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <iterator>
#include <algorithm>
#include <functional>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cstdio>

using namespace std;

#include "svm-svr.h"
#include "loadup_parameters.h"

void parse_command_line(int, char **,svmparam *);


int main(int argc,char *argv[]) {
  svmparam kr;
  FILE *ar=NULL;

  if (argc<4)  bad_cmd();

  parse_command_line(argc, argv,&kr);

  svm_svr A(kr); 

  if (kr.ftestset) {
    A.loadtest(kr);
  }
  vector<int> cl=A.get_allclasses();

  open_output_file(&ar,kr);

  REAL overall_error=0.0;
  REAL overall_rms=0.0;
  REAL overall_correlation=0.0;
  int Ne=0;

  for(int i=0;i<(int)cl.size();++i) {

    dichotomy D(cl[i],A,kr);
    if (!kr.modelfile_present) {
      printf("\n\nSolving dichotomy and regression  for class %d\n",cl[i]);
      printf("\n\nThe numbers in parenthesis are the upper bound of the KKT conditions and the # of support vectors\nYou can change the tolerance with -T\n");
      D.method2_svmr();
      if (ar!=NULL) fprintf(ar,"%d\t%4.5f%%\t%4.3f\t%f\n",i,D.error_rate()*100,D.rms(),D.pearson_correlation());
    } else {
      // transfer the solution loaded from file
      puts("trasnfering the solution");
      D.placesolution(kr,A,cl[i]);
      puts("solution placed");
    }

    if (kr.ftestset) {
      D.formtestset(cl[i],A,kr);
      REAL error=D.errortestsetsvmr();
      D.transfer_test_results(A,cl[i]);
      overall_rms+=error;
      overall_error+=D.error_rate();
      overall_correlation+=D.pearson_correlation();
      if (ar!=NULL) fprintf(ar,"(T)%d\t%4.5f%%\t%4.3f\t%f\n\n",i,D.error_rate()*100,D.rms(),D.pearson_correlation());
      ++Ne;
    }
    if (!kr.modelfile_present) D.transfersolution(kr,A);
  }

  if (!kr.modelfile_present) A.savesolution(kr);

  if (kr.ftestset) {
    if (ar!=NULL) {
       fprintf(ar,"\nOVERALL TEST SET RESULTS\n");
       fprintf(ar,"Classification error directly from dichotomy\t=%f%%\nRoot mean square\t=%f\nPearson Correlation\t=%f\n\n",overall_error/(REAL)Ne*100,overall_rms/(REAL)Ne,overall_correlation/(REAL)Ne);

       REAL unclassified,success;
       kr.offset=-1.5;
       do {
	 success=A.evaluate_test(unclassified,kr.offset);
	 fprintf(ar,"b-offset=%4.2f\tAccuracy=%4.2f%%\tUnclassified Patterns %4.2f%%\n",kr.offset,success,unclassified);
	 
	 kr.offset+=0.1;
       } while (unclassified>0.01);

     }
     printf("********************************************\nClassification error= %f%%\nRMS error=%f and Correlation=%f\n********************************************\n",overall_error/(REAL)Ne*100,overall_rms/(REAL)Ne,overall_correlation/(REAL)Ne);
   }


  cl.clear();
  puts("Exiting SVM-R");
	
  if (ar!=NULL) fclose(ar);

  return 1;
}


void parse_command_line(int argc, char **argv,svmparam *param)
{

	// default values

	param->problem_type="SVMR";
	param->pdegree = 3;
	param->c0 = 0;
	param->C = 1.0;
	param->ratio=1.0;
	param->tol = 1e-3;
	param->debug = false;
	param->ftestset =false; // the test set is not present by default
	param->svr_only =false;
	param->svm_only =false;
	param->epsilon =0.1;
	param->outputfile="";
	param->modelfile="";
	param->modelfile_present=false;
	param->normalization=true;
	param->turbo=false;

	parse_cmd(argc,argv,param);

	if ((param->modelfile_present==true)&&(param->modelfile.size()<2)) {
	  puts("Do not forget to indicate the model file with -m");
	  exit(0);
	}
	
}

