/**************************************************************/
/************* Inhibitory SVM *********************************/
/**************************************************************/
/*    Ramon Huerta/ Charles Elkan  ****************************/

/* GRANT: ONR N00014-07-1-0741                                */
/**************************************************************/


#define MMIN(X,Y) (((X)<(Y))?(X):(Y))
#define MMAX(X,Y) (((X)>(Y))?(X):(Y))
#define ABS(X)    (((X)>0)?(X):-(X))
#define SGN(X)    (((X)>=0)?(1.0):(-1.0))
#define EPS       (1e-10)  // Limit of resolution
#define MAXITER   100000000 // Limit in the # of iterations
#define COUNTER   2000   // progress indicator
#define KOFFSET   0.0
#define MAX_HESS  15

typedef double REAL; // More precission makes the algorithm converge faster
                     // Nevertheless try it out on your machine
//typedef float REAL;

// Data Storage type
typedef struct {
  int cls; // Class type
  REAL d; // data point value that is used for the regression
  int  Nt;       // # of points in the time series
  vector<REAL>  F;
} field; 

// Storage of the optimal cross validation values
typedef struct {
  REAL g;
  REAL C;
} optimal;

 // This data structures will be only used locally
  typedef struct {
    int cls; // Class type
    REAL d; // data point value that is used for the regression
    map<int,map<REAL,REAL> > F;// Features;
    vector<REAL> rF;
    int M; // Number of features per data point;
    int  Nt; // # of points in the time series

    } fieldlocal; 


typedef struct {
  int N;              // Number of points per class
  vector<REAL> mean; // Mean for each feature
  vector<REAL> std;  // Std of each feature
} stats;

// Kernel types
enum {RBF};

typedef struct {
  int    kernel_type; // Select function
  int    pdegree;
  REAL   gamma;
  REAL   c0;
  REAL   C;   
  REAL   tol;
  bool   debug; 
  bool   ftestset; // true for datatest present;
  bool   svm_only;
  bool   modelfile_present;
  string inputfile;
  string testfile;
  string outputfile;
  string modelfile;
  bool   normalization;
  REAL   offset;
  int    iterations;
  REAL   etafactor;
  int    learning_type;
  REAL   termination;
  int    nele;
  bool   platt;
  REAL   psplit;
    long nsplit;
  REAL   inhibitory;
} svmparam;

class multiclass;

class isvm {
 private:
  string problem_type;          // SVM,SVR or SVMR
  vector<field> data;           // Data storage
  int N;                        // Number of data points  
  int M;                        // Dimension of the feature vector
  int L;                        // Number of classes
  REAL eta;                     // Learning rate for SGD
  int  ltipo;                   // learning type for sgd
  REAL termination;             // Termination value for SGD

  int Ninfile;                  // Number of point in the file
  set<int> LNtrain,Vtrain;      // For cross-validation purporses
  set<int> LNtest,Vtest;

  //  map<int, stats> Stats;        // Basic statistics of the data per class (class-> stats)
  vector<int>  CC;               // Class labels
  set<int> inclass;

  map<int,int> mapa_reverse;     // map from classfier label to class label in data
  stats     ALL_dataset;        // Overall stats of the data set 
  int minfeature,maxfeature;
  int shift;

  vector<field> datatest;       // Data for the test set 
  int Ntest;                    // Number of data points  
  int Mtest;                    // Dimension of the feature vector
  int Ntestc;                   // Number of classes
  bool Ftestset;                // True if test file is present for the test file
  REAL offset;                  // Offset value for the multicalssification problem
  double maxtime;
  

  vector<int> blank_vectors;    // Vectors with blank features
  vector<vector<int> > blanks;  // features with blanks

  // The support vectors
  vector<REAL>  A;           // weights
  vector<vector<REAL> > ASV; // SVs
  vector<int>   iA;          // class function corresponde

  // Results of classifrying the test set
  vector<REAL>   C_test; // for classification
  // Output file with the results 
  string output_result;

 public:
  isvm(svmparam & ); // Load up the data
  isvm(string &);
  void loadtest(const svmparam & );
  vector<int> get_allclasses();
  int load_file(const string & filename, vector<fieldlocal> & ,set<int> &, REAL &);
   int load_file_spec(const string & , vector<fieldlocal> & ,set<int> &, REAL &);
  ~isvm();   
 
  void push_all(vector<REAL> &, vector<vector<REAL> > &,vector<int> &,map<int,int> &);
  void savesolution(const svmparam & );
  void load_model(svmparam &);
  void modelparse(string &,int &,REAL &,int &, svmparam &); 
  vector<REAL> getA();
  vector<int> getiA();
  vector<vector<REAL> > getSV();
  void loadup_testresults(vector<REAL> &);
  REAL evaluate_test(REAL &,REAL);
  int preload(const string &);
  optimal cross(svmparam &);
  void shuffle(svmparam &,int);
  void format(svmparam & );
  void loadtest_cross(const svmparam & );
  void preselect(svmparam & );


  friend class multiclass;

};

 

class multiclass {
 private:
  int  kernel_type;      
  REAL C;                 // The cost value for SVM 
  REAL Cold;              // C value in the previous iteration
  REAL tol;               // Tolerance level for verification of the KKT conditions
  int N;                   // Number of data points in the training set
  int M;                   // Number of features
  int L;                   // Number of classes

  int  maxiterations;      // Max # of iterations
  REAL eta;                // Leraning rate for the sgd <=1/C
  REAL factor;
 
  long iterations;
  bool track_progress;     // Turn this one to true if you want to track progress
                           // We will use this pointer to track progress of the SMO
  bool platt;              // true with Platt calibration
  FILE *aar;

  vector<REAL> ahat; // $\hat{\alpha}, \alpha, \alpha'$
  set<int> iahat;  // index of nonzero vectors
  int iskip;
  vector<int> skip; //skip alphas for LOO

  REAL error_sum;
  REAL *y;         // y=-1, y=1;
  int  *iy;
  vector<int> CC;
  vector<int> cls;
  map<int,int> mapa;       // map from class to classifier 
  map<int,int> mapa_reverse;// map from classfier to class
  REAL mu;

  REAL **x;
  //vector<vector<REAL> >   x;         // data points;
  vector<vector<int> >  snz;         // Nonzero data points
  vector<int>          Nsnz;         // Number of nonzero data points
  vector<int>        i_svmr;         // index of the positive examples

  vector<vector<REAL> > SVALL;
  vector<vector<int> >  ALLsnz;         // Nonzero data points
  vector<int>          NALLsnz;         // Number of nonzero data points
  vector<REAL>  aALL;
  vector<int>   iA;                  // We need to keep track of the class the SV eblong to for G_kk'
  map<int,vector<int> > cA;          // map with the index of the SVs corresponding to a class

  REAL *acopy;                       // a values for global updates

  REAL error_trainset;
  REAL error_svm;
  //Calibration
  map<int,REAL> C_A;
  map<int,REAL> C_B;

  //Test set 
  vector<REAL>           ytest; // y=-1, y=1;
  vector<vector<REAL> >  xtest; // data points;
  vector<vector<int> > stestnz; // non-zero index 
  vector<REAL>          c_test; // results of the classification    

  // Cached Errors:
  REAL *Kc;
  REAL **K;
  REAL *E;
  REAL *V;
  int  *noKKT;
  int  nkkt;
  multimap<REAL,int,greater<REAL> > Vmax;
  set<int> bad;
  int selected;

  // Hessian calculations
  double *aa,*bb;
  int *keep;
  int *ipiv;

  // Kernel parameters
  REAL gamma;

  REAL krbf(int, int);
  REAL krbftest(int, int);


  typedef REAL (multiclass::*KERNEL)(int,int); 
  //  KERNEL kernel; 

  typedef void (multiclass::*LTYPE)(); 
  LTYPE learning_function;

    public:
  
  multiclass(isvm &,const svmparam & );
  ~multiclass();

  REAL FSVM(int);
  REAL Ftest(int,int,REAL *);
  REAL Ftest_pre(int,int,REAL *);
  REAL Ftest_simple(int,int,REAL *);

  void smo(int);                  // stochastic smo
  void solver(isvm &);         // Multiclass solver
  void solver_incC(isvm &); 
  REAL solverLOO(isvm &,REAL &);         // Multiclass solver

  void examinesvm_wise( );
  void examinesvm_best( );
  REAL errorrate();
  void updateallEs();                                            // update Es for SVMs
  REAL getK(int i,int j);
  void updateallVs(int&,REAL&);
  void formtestset(isvm &,const svmparam & );
  REAL errortestset(isvm & );
  REAL errortestset_simple(isvm & );
  void placesolution(const svmparam &, isvm &);
  REAL evalsvm();
  REAL error_rate();       
  REAL G(int,int);
  REAL GT(int ,int,int,REAL *);
  REAL GT_pre(int ,int,int,REAL *);
  REAL GT_simple(int ,int,REAL *);

  void calibration() ;
  void clear_datapoints();
  void hessian(int);
  void hessian_dgesv(int);
  void simpleupdateaVs(int &,REAL &);
  REAL solverLOO_average(isvm & ,REAL &,int);
};



