This software applies the ISVM framework on time series. The original paper is 
[Huerta, Vembu, Amigó & Nowotny](http://www.mitpressjournals.org/doi/abs/10.1162/NECO_a_00321)
and it tends to work better when the datasets are smaller.


Compile
----

```
$ make cross;
$ make svm;
$ make clean          # to remove all object files
```

Note: it needs  -llapack -lblas


Testing
---

```
$ cd test
```

Note that the data format is :

(label) (channel 1);(time):(value(time,sensor1)) ... (channel N);(time):(value(time, sensor2)

Example: 2 1;0.000000:102.325200 2;0.000000:54.016100 3;0.000000:51.520800 4;0.000000:32.146000 5;0.000000:36.207200 6;0.000000:49.992000 7;0.000000:80.181500 8;0.000000:97.722100 1;0.100000:102.325000 2;0.100000:53.989000 3;0.100000:51.512400 4;0.100000:32.146000 5;0.100000:36.196900 6;0.100000:49.984000 7;0.100000:80.198000 8;0.100000:97.699300 1;0.200000:102.319556 

First run 10-fold crossvalidation (results below on Intel(R) Core(TM)2 Quad  CPU   Q9450  @ 2.66GHz):
```
$ time ../bin/cross -i filter_train.0.dat  -o results.dat

real	10m54.816s
user	10m33.720s
sys	0m20.073s
```
Then, check the results
```
$ cat results.dat
Partial Best C=0.100000 g=0.010000 error=47.777778
Partial Best C=0.100000 g=1.000000 error=42.881111
Partial Best C=1.000000 g=1.000000 error=28.946510
Partial Best C=10.000000 g=1.000000 error=28.436228
Partial Best C=20.000000 g=1.000000 error=25.676177
Best C=20.000000 g=1.000000 E=25.676177
```

This means that the optimal 10-fold crossvalidated results are C=20, gamma=1
with error of 25%


Next run the ISVM on the optimal parameters
```
$ ./bin/svm -C 20 -g 1.0 -i filter_train.0.dat -I filter_test.0.dat -o res.dat
```

Finally, 
```
$ cat res.dat
OVERALL TEST SET RESULTS
b-offset=-0.05	Accuracy=87.78%	Unclassified Patterns 0.00%
```